
# Description:

- First Screen Shows a little menu to select which movies to show, between Popular movies and Favorite Movies.
- You can select each movie to see its details.
- On the Details Window you can click on the heart button to make the movie a favorite movie, then it will be displayed as an item on the Favorites Movies List.
- Favorite movies are saved on a database using Room

## Architecture:

- MVVM
- S.O.L.I.D. Principles
- Clean Architecture
- 1 Activity Multiple fragments using Navigation Component

## Libraries Used:

- Kotlin Coroutines
- Data Binding
- Navigation Components
- Room for Data Persistence
- Retrofit for API Endpoint Fetching

## Notes:

- I decided not to use Dependency Injection on this project, but the classes' constructors are organized in a way that Dependency Injection can be easily implemented. If you want to see Dependency Injection in another project, you can go to https://gitlab.com/RoyGF/flink_test and https://github.com/RoyGF/RappiTest

## ScreenShots:

![Screenshot_20210131-131604 2](https://user-images.githubusercontent.com/16839948/106395246-cbdf1000-63c6-11eb-936c-299a1ef6404d.png)
![Screenshot_20210131-131516 2](https://user-images.githubusercontent.com/16839948/106395242-c5e92f00-63c6-11eb-99af-faf97d8cf91c.png)
![Screenshot_20210131-131423](https://user-images.githubusercontent.com/16839948/106395243-c8e41f80-63c6-11eb-85ac-a07e10e4a2aa.png)
