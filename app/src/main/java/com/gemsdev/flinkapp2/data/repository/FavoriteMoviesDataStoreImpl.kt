package com.gemsdev.flinkapp2.data.repository

import com.gemsdev.flinkapp2.data.db.MoviesDatabase
import com.gemsdev.flinkapp2.data.entities.MovieData
import com.gemsdev.flinkapp2.data.mappers.Mapper
import com.gemsdev.flinkapp2.domain.entities.MovieEntity
import com.gemsdev.flinkapp2.domain.repository.FavoritesDataStore

class FavoriteMoviesDataStoreImpl(
    private val moviesDatabase: MoviesDatabase,
    private val movieEntityDataMapper: Mapper<MovieEntity, MovieData>,
    private val movieDataEntityMapper: Mapper<MovieData, MovieEntity>
) : FavoritesDataStore {

    private val dao = moviesDatabase.getMoviesDao()

    override suspend fun getFavoriteMovies(): List<MovieEntity> {
        val data = dao.getFavorites()
        return data.map { movieDataEntityMapper.mapFrom(it) }
    }

    override suspend fun saveFavoriteMovie(movieEntity: MovieEntity) {
        dao.saveMovie(movieEntityDataMapper.mapFrom(movieEntity))
    }

    override suspend fun removeFavoriteMovie(movieEntity: MovieEntity) {
        dao.removeMovie(movieEntityDataMapper.mapFrom(movieEntity))
    }

    override suspend fun getFavoriteMovie(movieId: Int): MovieEntity? {
        val data = dao.get(movieId)
        data?.let {
            return movieDataEntityMapper.mapFrom(it)
        } ?: return null
    }

}