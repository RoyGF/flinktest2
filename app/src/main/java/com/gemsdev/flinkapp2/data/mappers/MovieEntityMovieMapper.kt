package com.gemsdev.flinkapp2.data.mappers

import com.gemsdev.flinkapp2.domain.entities.MovieEntity
import com.gemsdev.flinkapp2.presentation.entities.Movie


class MovieEntityMovieMapper() : Mapper<MovieEntity, Movie>() {

    companion object {
        const val posterBaseUrl = "https://image.tmdb.org/t/p/w342"
        const val backdropBaseUrl = "https://image.tmdb.org/t/p/w780"
    }

    override fun mapFrom(from: MovieEntity): Movie {
        val movie = Movie(
            id = from.id,
            voteCount = from.voteCount,
            video = from.video,
            voteAverage = from.voteAverage,
            title = from.title,
            popularity = from.popularity,
            originalLanguage = from.originalLanguage,
            posterPath = from.posterPath?.let {
                if (it.contains("http")) {
                    from.posterPath
                } else {
                    posterBaseUrl + from.posterPath
                }
            },
            backdropPath = from.backdropPath?.let { backdropBaseUrl + from.backdropPath },
            originalTitle = from.originalTitle,
            adult = from.adult,
            releaseDate = from.releaseDate,
            overview = from.overview,
            isFavorite = from.isFavorite
        )

        return movie
    }
}