package com.gemsdev.flinkapp2.data.repository

import com.gemsdev.flinkapp2.data.api.Api
import com.gemsdev.flinkapp2.data.entities.MovieData
import com.gemsdev.flinkapp2.data.entities.MovieDetailsData
import com.gemsdev.flinkapp2.data.mappers.Mapper
import com.gemsdev.flinkapp2.domain.entities.MovieEntity
import com.gemsdev.flinkapp2.domain.repository.MoviesRepository
import org.json.JSONObject

class RemoteMoviesImpl(
    private val api: Api,
    private val movieDataEntityMapper: Mapper<MovieData, MovieEntity>,
    private val detailsMapper: Mapper<MovieDetailsData, MovieEntity>
) : MoviesRepository {

    override suspend fun getMovies(): List<MovieEntity> {
        val apiResult = api.getPopularMovies()
        return apiResult.movies.map { movieDataEntityMapper.mapFrom(it) }
    }

    override suspend fun getMovies(pagination: Int): List<MovieEntity> {
        val apiResult = api.getPopularMovies(pagination)
        return apiResult.movies.map { movieDataEntityMapper.mapFrom(it) }
    }

    override suspend fun getMovie(movieId: Int): MovieEntity? {
        val details = api.getMovieDetails(movieId)
        return detailsMapper.mapFrom(details)
    }

}