package com.gemsdev.flinkapp2.data.db

import android.content.Context
import androidx.room.Room

object DatabaseBuilder {
    private var instance: MoviesDatabase? = null

    fun getInstance(context: Context): MoviesDatabase {
        if (instance == null) {
            synchronized(MoviesDatabase::class) {
                instance = buildDatabase(context)
            }
        }
        return instance!!
    }

    private fun buildDatabase(context: Context) =
        Room.databaseBuilder(context, MoviesDatabase::class.java, "movies_db").build()
}