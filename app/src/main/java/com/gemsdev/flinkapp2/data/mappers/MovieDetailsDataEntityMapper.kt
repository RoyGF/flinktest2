package com.gemsdev.flinkapp2.data.mappers

import com.gemsdev.flinkapp2.data.entities.MovieDetailsData
import com.gemsdev.flinkapp2.domain.entities.MovieEntity

class MovieDetailsDataEntityMapper() : Mapper<MovieDetailsData, MovieEntity>() {

    override fun mapFrom(from: MovieDetailsData): MovieEntity {
        val movieEntity = MovieEntity(
            id = from.id,
            voteCount = from.voteCount,
            video = from.video,
            voteAverage = from.voteAverage,
            popularity = from.popularity,
            adult = from.adult,
            title = from.title,
            posterPath = from.posterPath,
            originalTitle = from.originalTitle,
            backdropPath = from.backdropPath,
            originalLanguage = from.originalLanguage,
            releaseDate = from.releaseDate,
            overview = from.overview,
            isFavorite = from.isFavorite
        )
        return movieEntity
    }

}