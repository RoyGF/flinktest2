package com.gemsdev.flinkapp2.data.mappers

import com.gemsdev.flinkapp2.domain.entities.MovieEntity
import com.gemsdev.flinkapp2.presentation.entities.Movie

class MovieObjectEntityMapper() : Mapper<Movie, MovieEntity>() {

    override fun mapFrom(from: Movie): MovieEntity {
        return MovieEntity(
            id = from.id,
            voteCount = from.voteCount,
            voteAverage = from.voteAverage,
            popularity = from.popularity,
            adult = from.adult,
            title = from.title,
            posterPath = from.posterPath,
            originalLanguage = from.originalLanguage,
            backdropPath = from.backdropPath,
            originalTitle = from.originalTitle,
            releaseDate = from.releaseDate,
            overview = from.overview,
            isFavorite = from.isFavorite
        )
    }
}