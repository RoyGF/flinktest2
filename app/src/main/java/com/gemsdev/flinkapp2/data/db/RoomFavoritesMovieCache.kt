package com.gemsdev.flinkapp2.data.db

import com.gemsdev.flinkapp2.data.entities.MovieData
import com.gemsdev.flinkapp2.data.mappers.Mapper
import com.gemsdev.flinkapp2.domain.repository.MoviesCache
import com.gemsdev.flinkapp2.domain.entities.MovieEntity

class RoomFavoritesMovieCache(
    database: MoviesDatabase,
    private val entityToDataMapper: Mapper<MovieEntity, MovieData>,
    private val dataToEntityMapper: Mapper<MovieData, MovieEntity>
) : MoviesCache {

    private val dao: MoviesDao = database.getMoviesDao()

    override suspend fun clear() {
        dao.clear()
    }

    override suspend fun save(movieEntity: MovieEntity) {
        dao.saveMovie(entityToDataMapper.mapFrom(movieEntity))
    }

    override suspend fun remove(movieEntity: MovieEntity) {
        dao.removeMovie(entityToDataMapper.mapFrom(movieEntity))
    }

    override suspend fun saveAll(movieEntities: List<MovieEntity>) {
        dao.saveAllMovies(movieEntities.map { entityToDataMapper.mapFrom(it) })
    }

    override suspend fun getAll(): List<MovieEntity> {
        return dao.getFavorites().map { dataToEntityMapper.mapFrom(it) }
    }

    override suspend fun get(movieId: Int): MovieEntity? {
        val data = dao.get(movieId)
        data?.let {
            return dataToEntityMapper.mapFrom(data)
        } ?: return null
    }

    override suspend fun isEmpty(): Boolean {
        return dao.getFavorites().isEmpty()
    }

}