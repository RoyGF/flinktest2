package com.gemsdev.flinkapp2.data.repository

import com.gemsdev.flinkapp2.domain.repository.MoviesCache
import com.gemsdev.flinkapp2.domain.entities.MovieEntity

class MovieCacheImpl : MoviesCache {

    private val moviesCache: LinkedHashMap<Int, MovieEntity> = LinkedHashMap()

    override suspend fun clear() {
        moviesCache.clear()
    }

    override suspend fun save(movieEntity: MovieEntity) {
        moviesCache[movieEntity.id] = movieEntity
    }

    override suspend fun remove(movieEntity: MovieEntity) {
        moviesCache.remove(movieEntity.id)
    }

    override suspend fun saveAll(movieEntities: List<MovieEntity>) {
        movieEntities.forEach { movieEntity -> this.moviesCache[movieEntity.id] = movieEntity }
    }

    override suspend fun getAll(): List<MovieEntity> {
        return moviesCache.values.toList()
    }

    override suspend fun get(movieId: Int): MovieEntity? {
        return moviesCache[movieId]
    }

    override suspend fun isEmpty(): Boolean {
        return moviesCache.isEmpty()
    }
}