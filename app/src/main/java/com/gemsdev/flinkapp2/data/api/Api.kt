package com.gemsdev.flinkapp2.data.api

import com.gemsdev.flinkapp2.data.entities.MovieDetailsData
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface Api {
    @GET("movie/popular")
    suspend fun getPopularMovies(): MovieListResult

    @GET("movie/popular")
    suspend fun getPopularMovies(@Query("page") page: Int): MovieListResult

    @GET("movie/{id}")
    suspend fun getMovieDetails(@Path("id") movieId: Int): MovieDetailsData
}