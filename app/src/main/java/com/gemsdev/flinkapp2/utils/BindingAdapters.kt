package com.gemsdev.flinkapp2.utils

import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.gemsdev.flinkapp2.R
import com.google.android.material.floatingactionbutton.FloatingActionButton

/**
 * Binds Adapter into a RecyclerView
 */
@BindingAdapter("adapter")
fun setAdapter(recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>) {
    recyclerView.adapter = adapter
}

/**
 * Binds movie image into an ImageView
 */
@BindingAdapter("movieImage")
fun loadImage(imageView: ImageView, imagePath: String?) {
    if (imagePath != null) {
        Glide.with(imageView.context)
            .load(imagePath)
            .into(imageView)
    }
}

/**
 * Binds Text into a TextView
 */
@BindingAdapter("textValue")
fun loadText(textView: TextView, textValue: String?) {
    if (textValue != null) {
        textView.text = textValue
    } else {
        textView.text = "Non_text_available"
    }
}

@BindingAdapter("favorite_icon")
fun setIcon(actionButton: FloatingActionButton, favorite: Boolean) {
    actionButton.setImageDrawable(
        if (favorite) {
            ContextCompat.getDrawable(actionButton.context, R.drawable.ic_favorite_selected)
        } else {
            ContextCompat.getDrawable(actionButton.context, R.drawable.ic_favorite_unselected)
        }
    )
}

@BindingAdapter("vote_average")
fun setVote(textView: TextView, value: Double) {
    val text = value.toString()
    textView.text = text
}

