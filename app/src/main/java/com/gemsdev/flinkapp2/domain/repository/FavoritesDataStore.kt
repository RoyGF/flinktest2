package com.gemsdev.flinkapp2.domain.repository

import com.gemsdev.flinkapp2.domain.entities.MovieEntity

interface FavoritesDataStore {
    suspend fun getFavoriteMovies(): List<MovieEntity>
    suspend fun saveFavoriteMovie(movieEntity: MovieEntity)
    suspend fun removeFavoriteMovie(movieEntity: MovieEntity)
    suspend fun getFavoriteMovie(movieId: Int): MovieEntity?
}