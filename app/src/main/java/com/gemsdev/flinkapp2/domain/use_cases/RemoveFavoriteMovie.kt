package com.gemsdev.flinkapp2.domain.use_cases

import com.gemsdev.flinkapp2.base.UseCase
import com.gemsdev.flinkapp2.domain.repository.FavoritesDataStore
import com.gemsdev.flinkapp2.domain.entities.MovieEntity

class RemoveFavoriteMovie(private val dataStore: FavoritesDataStore) : UseCase() {
    suspend fun remove(movieEntity: MovieEntity) {
        dataStore.removeFavoriteMovie(movieEntity)
    }
}