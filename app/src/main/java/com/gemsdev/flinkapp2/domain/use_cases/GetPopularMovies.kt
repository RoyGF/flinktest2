package com.gemsdev.flinkapp2.domain.use_cases

import com.gemsdev.flinkapp2.base.UseCase
import com.gemsdev.flinkapp2.domain.entities.MovieEntity
import com.gemsdev.flinkapp2.domain.repository.MoviesRepository

class GetPopularMovies(private val repository: MoviesRepository) : UseCase() {
    suspend fun get(): List<MovieEntity> {
        return repository.getMovies()
    }

    suspend fun get(page: Int): List<MovieEntity> {
        return repository.getMovies(page)
    }
}