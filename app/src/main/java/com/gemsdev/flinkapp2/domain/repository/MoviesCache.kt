package com.gemsdev.flinkapp2.domain.repository

import com.gemsdev.flinkapp2.domain.entities.MovieEntity

interface MoviesCache {
    suspend fun clear()
    suspend fun save(movieEntity: MovieEntity)
    suspend fun remove(movieEntity: MovieEntity)
    suspend fun saveAll(movieEntities: List<MovieEntity>)
    suspend fun getAll(): List<MovieEntity>
    suspend fun get(movieId: Int): MovieEntity?
    suspend fun isEmpty(): Boolean
}