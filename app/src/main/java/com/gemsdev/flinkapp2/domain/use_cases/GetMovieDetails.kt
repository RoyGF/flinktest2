package com.gemsdev.flinkapp2.domain.use_cases

import com.gemsdev.flinkapp2.base.UseCase
import com.gemsdev.flinkapp2.domain.entities.MovieEntity
import com.gemsdev.flinkapp2.domain.repository.FavoritesDataStore
import com.gemsdev.flinkapp2.domain.repository.MoviesRepository

class GetMovieDetails(
    private val moviesRepository: MoviesRepository,
    private val favoritesDataStore: FavoritesDataStore
) : UseCase() {
    suspend fun getMovieById(movieId: Int): MovieEntity? {
        val data = favoritesDataStore.getFavoriteMovie(movieId)
        data?.let {
            return it
        } ?: return moviesRepository.getMovie(movieId)
    }
}