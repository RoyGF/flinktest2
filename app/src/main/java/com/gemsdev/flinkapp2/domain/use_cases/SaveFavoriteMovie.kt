package com.gemsdev.flinkapp2.domain.use_cases

import com.gemsdev.flinkapp2.base.UseCase
import com.gemsdev.flinkapp2.domain.repository.FavoritesDataStore
import com.gemsdev.flinkapp2.domain.entities.MovieEntity

class SaveFavoriteMovie(private val dataStore: FavoritesDataStore): UseCase() {

    companion object {
        private const val PARAM_MOVIE_ENTITY = "param:movieEntity"
    }

    suspend fun saveFavoriteMovie(movieEntity: MovieEntity) {
        dataStore.saveFavoriteMovie(movieEntity)
    }

}