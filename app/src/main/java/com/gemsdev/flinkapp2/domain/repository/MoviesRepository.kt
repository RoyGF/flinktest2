package com.gemsdev.flinkapp2.domain.repository

import com.gemsdev.flinkapp2.domain.entities.MovieEntity

interface MoviesRepository {
    suspend fun getMovies(): List<MovieEntity>
    suspend fun getMovie(movieId: Int): MovieEntity?
    suspend fun getMovies(pagination: Int): List<MovieEntity>
}