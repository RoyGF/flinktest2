package com.gemsdev.flinkapp2.domain.entities

data class GenreEntity(
    var id: Int,
    var name: String
)