package com.gemsdev.flinkapp2.presentation.ui.movie_list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.gemsdev.flinkapp2.R
import com.gemsdev.flinkapp2.databinding.ItemMovieBinding
import com.gemsdev.flinkapp2.presentation.entities.Movie

class MovieListAdapter :
    RecyclerView.Adapter<MovieListAdapter.MovieViewHolder>(), View.OnClickListener {

    private val movies: MutableList<Movie> = mutableListOf()
    private lateinit var listener: MovieListListener

    class MovieViewHolder(
        val binding: ItemMovieBinding,
        private val clickListener: View.OnClickListener
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(movie: Movie) {
            binding.movie = movie
            binding.root.setOnClickListener(clickListener)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val binding: ItemMovieBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_movie, parent, false
        )
        return MovieViewHolder(binding, this)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movie = movies[position]
        holder.bind(movie)
        holder.binding.root.tag = movie
    }

    fun addMovies(movies: List<Movie>) {
        this.movies.addAll(movies)
        notifyDataSetChanged()
    }

    override fun onClick(view: View?) {
        val movie = view?.tag as Movie
        listener.onMovieClick(movie)
    }

    fun setListener(movieListener: MovieListListener) {
        listener = movieListener
    }

    interface MovieListListener {
        fun onMovieClick(movie: Movie)
    }
}