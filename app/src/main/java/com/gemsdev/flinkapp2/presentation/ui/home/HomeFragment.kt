package com.gemsdev.flinkapp2.presentation.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.gemsdev.flinkapp2.R
import com.gemsdev.flinkapp2.base.ListType
import com.gemsdev.flinkapp2.databinding.FragmentHomeBinding
import com.gemsdev.flinkapp2.presentation.ui.movie_list.MovieListFragment

class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        init()
        return binding.root
    }

    private fun init() {
        binding.buttonPopular.setOnClickListener {
            val bundle = bundleOf(MovieListFragment.EXTRA_LIST_TYPE to ListType.POPULAR)
            Navigation.findNavController(binding.root)
                .navigate(R.id.action_homeFragment_to_popularMoviesFragment, bundle)
        }
        binding.buttonFavorites.setOnClickListener {
            val bundle = bundleOf(MovieListFragment.EXTRA_LIST_TYPE to ListType.FAVORITES)
            Navigation.findNavController(binding.root)
                .navigate(R.id.action_homeFragment_to_popularMoviesFragment, bundle)
        }
    }
}
