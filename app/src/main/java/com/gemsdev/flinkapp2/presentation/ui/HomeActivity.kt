package com.gemsdev.flinkapp2.presentation.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.gemsdev.flinkapp2.R

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }
}
