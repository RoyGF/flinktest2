package com.gemsdev.flinkapp2.presentation.ui.movie_details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gemsdev.flinkapp2.data.mappers.Mapper
import com.gemsdev.flinkapp2.domain.entities.MovieEntity
import com.gemsdev.flinkapp2.domain.use_cases.GetMovieDetails
import com.gemsdev.flinkapp2.domain.use_cases.RemoveFavoriteMovie
import com.gemsdev.flinkapp2.domain.use_cases.SaveFavoriteMovie
import com.gemsdev.flinkapp2.presentation.entities.Movie

class MovieDetailsViewModelFactory(
    private val getMovieDetails: GetMovieDetails,
    private val saveFavoriteMovie: SaveFavoriteMovie,
    private val removeFavoriteMovie: RemoveFavoriteMovie,
    private val entityMovieMapper: Mapper<MovieEntity, Movie>,
    private val movieEntityMapper: Mapper<Movie, MovieEntity>
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MovieDetailsViewModel::class.java)) {
            return MovieDetailsViewModel(
                getMovieDetails,
                saveFavoriteMovie,
                removeFavoriteMovie,
                entityMovieMapper,
                movieEntityMapper
            ) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}