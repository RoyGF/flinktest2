package com.gemsdev.flinkapp2.presentation.ui.movie_details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.gemsdev.flinkapp2.base.Resource
import com.gemsdev.flinkapp2.data.mappers.Mapper
import com.gemsdev.flinkapp2.domain.entities.MovieEntity
import com.gemsdev.flinkapp2.domain.use_cases.GetMovieDetails
import com.gemsdev.flinkapp2.domain.use_cases.RemoveFavoriteMovie
import com.gemsdev.flinkapp2.domain.use_cases.SaveFavoriteMovie
import com.gemsdev.flinkapp2.presentation.entities.Movie
import kotlinx.coroutines.Dispatchers

class MovieDetailsViewModel(
    private val getMovieDetails: GetMovieDetails,
    private val saveFavoriteMovie: SaveFavoriteMovie,
    private val remoteFavoriteMovie: RemoveFavoriteMovie,
    private val entityMovieMapper: Mapper<MovieEntity, Movie>,
    private val movieEntityMapper: Mapper<Movie, MovieEntity>
) : ViewModel() {

    fun getMovieDetails(movieId: Int) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            val data = getMovieDetails.getMovieById(movieId)
            data?.let {
                emit(Resource.success(data = entityMovieMapper.mapFrom(it)))
            }
        } catch (exception: Exception) {
            emit(
                Resource.error(
                    data = null,
                    message = exception.message ?: "Could not get movie Detals"
                )
            )
        }
    }

    fun saveFavoriteMovie(movie: Movie) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            movie.isFavorite = true
            saveFavoriteMovie.saveFavoriteMovie(movieEntityMapper.mapFrom(movie))
            emit(Resource.success(data = true))
        } catch (exception: Exception) {
            emit(
                Resource.error(
                    data = null,
                    message = exception.message ?: "Could not save movie"
                )
            )
        }
    }

    fun removeFavoriteMovie(movie: Movie) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            remoteFavoriteMovie.remove(movieEntityMapper.mapFrom(movie))
            emit(Resource.success(data = false))
        } catch (exception: Exception) {
            emit(
                Resource.error(
                    data = null,
                    message = exception.message ?: "Could not remove Favorite Movie"
                )
            )
        }
    }
}
