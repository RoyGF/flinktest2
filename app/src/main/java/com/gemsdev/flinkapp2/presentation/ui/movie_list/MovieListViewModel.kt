package com.gemsdev.flinkapp2.presentation.ui.movie_list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.gemsdev.flinkapp2.base.Resource
import com.gemsdev.flinkapp2.data.mappers.Mapper
import com.gemsdev.flinkapp2.domain.entities.MovieEntity
import com.gemsdev.flinkapp2.domain.use_cases.GetFavoriteMovies
import com.gemsdev.flinkapp2.domain.use_cases.GetPopularMovies
import com.gemsdev.flinkapp2.presentation.entities.Movie
import kotlinx.coroutines.Dispatchers

class MovieListViewModel(
    private val getPopularMovies: GetPopularMovies,
    private val getFavoriteMovies: GetFavoriteMovies,
    private val mapper: Mapper<MovieEntity, Movie>
) : ViewModel() {

    private var pagination = 1

    fun getMovies() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            val data = getPopularMovies.get()
            emit(Resource.success(data = data.map { mapper.mapFrom(it) }))
        } catch (exception: Exception) {
            emit(
                Resource.error(
                    data = null,
                    message = exception.message ?: "Could not retrieve movies"
                )
            )
        }
    }

    fun loadMoreMovies() = liveData(Dispatchers.IO) {
        pagination += 1
        emit(Resource.loading(data = null))
        try {
            val data = getPopularMovies.get(pagination)
            emit(Resource.success(data = data.map { mapper.mapFrom(it) }))
        } catch (exception: Exception) {
            emit(
                Resource.error(
                    data = null,
                    message = exception.message ?: "Could not retrieve movies"
                )
            )
        }
    }

    fun getFavoriteMovies() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            val data = getFavoriteMovies.getMovies()
            emit(Resource.success(data = data.map { mapper.mapFrom(it) }))
        } catch (exception: Exception) {
            emit(
                Resource.error(
                    data = null,
                    message = exception.message ?: "Could not retrieve movies"
                )
            )
        }
    }

}

