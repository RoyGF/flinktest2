package com.gemsdev.flinkapp2.presentation.entities

data class Movie(
    var id: Int = 0,
    var voteCount: Int = 0,
    var video: Boolean = false,
    var voteAverage: Double = 0.0,
    var popularity: Double = 0.0,
    var adult: Boolean = false,
    var title: String,
    var posterPath: String?,
    var originalLanguage: String,
    var originalTitle: String,
    var backdropPath: String?,
    var releaseDate: String,
    var overview: String? = null,
    var isFavorite: Boolean = false
)