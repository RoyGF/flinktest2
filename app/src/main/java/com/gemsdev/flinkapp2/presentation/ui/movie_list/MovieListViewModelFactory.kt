package com.gemsdev.flinkapp2.presentation.ui.movie_list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gemsdev.flinkapp2.data.mappers.Mapper
import com.gemsdev.flinkapp2.domain.entities.MovieEntity
import com.gemsdev.flinkapp2.domain.use_cases.GetFavoriteMovies
import com.gemsdev.flinkapp2.domain.use_cases.GetPopularMovies
import com.gemsdev.flinkapp2.presentation.entities.Movie

class MovieListViewModelFactory(
    private val getPopularMovies: GetPopularMovies,
    private val getFavoriteMovies: GetFavoriteMovies,
    private val mapper: Mapper<MovieEntity, Movie>
) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MovieListViewModel::class.java)) {
            return MovieListViewModel(getPopularMovies, getFavoriteMovies, mapper) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}