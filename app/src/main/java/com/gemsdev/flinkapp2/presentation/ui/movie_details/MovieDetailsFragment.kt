package com.gemsdev.flinkapp2.presentation.ui.movie_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.gemsdev.flinkapp2.R
import com.gemsdev.flinkapp2.base.Status
import com.gemsdev.flinkapp2.data.api.ApiClient
import com.gemsdev.flinkapp2.data.db.DatabaseBuilder
import com.gemsdev.flinkapp2.data.mappers.*
import com.gemsdev.flinkapp2.data.repository.FavoriteMoviesDataStoreImpl
import com.gemsdev.flinkapp2.data.repository.RemoteMoviesImpl
import com.gemsdev.flinkapp2.databinding.MovieDetailsFragmentBinding
import com.gemsdev.flinkapp2.domain.use_cases.GetMovieDetails
import com.gemsdev.flinkapp2.domain.use_cases.RemoveFavoriteMovie
import com.gemsdev.flinkapp2.domain.use_cases.SaveFavoriteMovie
import com.gemsdev.flinkapp2.presentation.entities.Movie

class MovieDetailsFragment : Fragment() {

    companion object {
        val EXTRA_ID = "movieId"
    }

    private lateinit var viewModel: MovieDetailsViewModel
    private lateinit var factory: MovieDetailsViewModelFactory
    private lateinit var binding: MovieDetailsFragmentBinding
    private var movieId = -1
    private lateinit var movie: Movie

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        arguments?.let {
            movieId = it.get(EXTRA_ID) as Int
        }
        binding =
            DataBindingUtil.inflate(inflater, R.layout.movie_details_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViewModel()
        getMovieDetails(movieId)
        setListener()
        if (savedInstanceState == null) {
            viewModel.getMovieDetails(movieId)
        }
    }

    private fun initViewModel() {
        val repository = RemoteMoviesImpl(
            ApiClient.provideRetrofit(),
            MovieDataEntityMapper(),
            MovieDetailsDataEntityMapper()
        )
        val database = DatabaseBuilder.getInstance(requireContext())
        val dataStore =
            FavoriteMoviesDataStoreImpl(database, MovieEntityDataMapper(), MovieDataEntityMapper())
        factory = MovieDetailsViewModelFactory(
            GetMovieDetails(repository, dataStore),
            SaveFavoriteMovie(dataStore),
            RemoveFavoriteMovie(dataStore),
            MovieEntityMovieMapper(),
            MovieObjectEntityMapper()
        )
        viewModel = ViewModelProviders.of(this, factory).get(MovieDetailsViewModel::class.java)
    }

    private fun setListener() {
        binding.buttonFavorites.setOnClickListener {
            if (movie.isFavorite) {
                removeFavoriteMovie(movie)
            } else {
                saveFavoriteMovie(movie)
            }
        }
    }

    private fun getMovieDetails(inMovieId: Int) {
        viewModel.getMovieDetails(inMovieId).observe(viewLifecycleOwner, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        showProgress(false)
                        resource.data?.let { inMovie ->
                            binding.movie = inMovie
                            movie = inMovie
                        }
                    }
                    Status.ERROR -> {
                        showProgress(false)
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> {
                        showProgress(true)
                    }
                }
            }
        })
    }

    private fun saveFavoriteMovie(movie: Movie) {
        viewModel.saveFavoriteMovie(movie).observe(viewLifecycleOwner, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        showProgress(false)
                        resource.data?.let { isFavorite ->
                            movie.isFavorite = isFavorite
                            binding.movie = movie
                            Toast.makeText(
                                requireContext(),
                                "Guardado como favorito",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                    Status.ERROR -> {
                        showProgress(false)
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> {
                        showProgress(true)
                    }
                }
            }
        })
    }

    private fun removeFavoriteMovie(movie: Movie) {
        viewModel.removeFavoriteMovie(movie).observe(viewLifecycleOwner, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        showProgress(false)
                        resource.data?.let { isFavorite ->
                            movie.isFavorite = isFavorite
                            binding.movie = movie
                            Toast.makeText(
                                requireContext(),
                                "Se quitó como favorito",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                    Status.ERROR -> {
                        showProgress(false)
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> {
                        showProgress(true)
                    }
                }
            }
        })
    }

    private fun showProgress(visible: Boolean) {
        binding.progressBar.visibility = if (visible) View.VISIBLE else View.GONE
    }

}
