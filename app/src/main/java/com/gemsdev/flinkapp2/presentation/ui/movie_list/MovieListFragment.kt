package com.gemsdev.flinkapp2.presentation.ui.movie_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gemsdev.flinkapp2.R
import com.gemsdev.flinkapp2.base.ListType
import com.gemsdev.flinkapp2.base.Status
import com.gemsdev.flinkapp2.data.api.ApiClient
import com.gemsdev.flinkapp2.data.db.DatabaseBuilder
import com.gemsdev.flinkapp2.data.mappers.MovieDataEntityMapper
import com.gemsdev.flinkapp2.data.mappers.MovieDetailsDataEntityMapper
import com.gemsdev.flinkapp2.data.mappers.MovieEntityDataMapper
import com.gemsdev.flinkapp2.data.mappers.MovieEntityMovieMapper
import com.gemsdev.flinkapp2.data.repository.FavoriteMoviesDataStoreImpl
import com.gemsdev.flinkapp2.data.repository.RemoteMoviesImpl
import com.gemsdev.flinkapp2.databinding.PopularMoviesFragmentBinding
import com.gemsdev.flinkapp2.domain.use_cases.GetFavoriteMovies
import com.gemsdev.flinkapp2.domain.use_cases.GetPopularMovies
import com.gemsdev.flinkapp2.presentation.entities.Movie
import com.gemsdev.flinkapp2.presentation.ui.movie_details.MovieDetailsFragment

class MovieListFragment : Fragment(), MovieListAdapter.MovieListListener {

    companion object {
        val EXTRA_LIST_TYPE = "list_type"
    }

    private lateinit var factory: MovieListViewModelFactory
    private lateinit var viewModel: MovieListViewModel
    private lateinit var binding: PopularMoviesFragmentBinding
    private lateinit var recyclerView: RecyclerView
    private lateinit var listType: ListType
    private lateinit var movieListAdapter: MovieListAdapter
    private var canLoadMoreMovies = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        arguments?.let {
            listType = it.get(EXTRA_LIST_TYPE) as ListType
        }

        binding =
            DataBindingUtil.inflate(inflater, R.layout.popular_movies_fragment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
        setUpUI()
        getMovies()
        if (savedInstanceState == null) {
            canLoadMoreMovies = false
            viewModel.getMovies()
        }
    }

    private fun initViewModel() {
        val repository = RemoteMoviesImpl(
            ApiClient.provideRetrofit(),
            MovieDataEntityMapper(),
            MovieDetailsDataEntityMapper()
        )
        val database = DatabaseBuilder.getInstance(requireContext())
        val dataStore =
            FavoriteMoviesDataStoreImpl(database, MovieEntityDataMapper(), MovieDataEntityMapper())
        factory =
            MovieListViewModelFactory(
                GetPopularMovies(repository),
                GetFavoriteMovies(dataStore), MovieEntityMovieMapper()
            )
        viewModel = ViewModelProviders.of(this, factory).get(MovieListViewModel::class.java)
    }

    private fun setUpUI() {
        movieListAdapter = MovieListAdapter()
        movieListAdapter.setListener(this)
        recyclerView = binding.popularMoviesRecyclerview
        recyclerView.layoutManager = GridLayoutManager(activity, 2)
        recyclerView.adapter = movieListAdapter

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1)) {
                    if (!canLoadMoreMovies) {
                        return
                    }
                    loadMoreMovies()
                }
            }
        })
    }

    private fun getMovies() {
        if (listType == ListType.FAVORITES) {
            getFavoriteMovies()
        } else {
            getPopularMovies()
        }
    }

    private fun getFavoriteMovies() {
        viewModel.getFavoriteMovies().observe(viewLifecycleOwner, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        canLoadMoreMovies = true
                        showProgress(false)
                        resource.data?.let { movies -> manageMovies(movies) }
                    }
                    Status.ERROR -> {
                        canLoadMoreMovies = false
                        showProgress(false)
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> {
                        canLoadMoreMovies = false
                        showProgress(true)
                    }
                }
            }
        })
    }

    private fun getPopularMovies() {
        viewModel.getMovies().observe(viewLifecycleOwner, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        canLoadMoreMovies = true
                        showProgress(false)
                        resource.data?.let { movies -> manageMovies(movies) }
                    }
                    Status.ERROR -> {
                        canLoadMoreMovies = false
                        showProgress(false)
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> {
                        canLoadMoreMovies = false
                        showProgress(true)
                    }
                }
            }
        })
    }

    private fun loadMoreMovies() {
        if (listType == ListType.FAVORITES) {
            return
        }

        viewModel.loadMoreMovies().observe(viewLifecycleOwner, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        canLoadMoreMovies = true
                        showProgress(false)
                        resource.data?.let { movies -> manageMovies(movies) }
                    }
                    Status.ERROR -> {
                        showProgress(false)
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> {
                        showProgress(true)
                    }
                }
            }
        })
    }

    private fun manageMovies(movies: List<Movie>) {
        if (movies.isNotEmpty()) {
            movieListAdapter.addMovies(movies)
        }
    }

    private fun showProgress(visible: Boolean) {
        binding.popularMoviesProgress.visibility = if (visible) View.VISIBLE else View.GONE
    }

    override fun onMovieClick(movie: Movie) {
        val bundle = bundleOf(MovieDetailsFragment.EXTRA_ID to movie.id)
        Navigation.findNavController(binding.root)
            .navigate(R.id.action_popularMoviesFragment_to_movieDetailsFragment, bundle)
    }
}
