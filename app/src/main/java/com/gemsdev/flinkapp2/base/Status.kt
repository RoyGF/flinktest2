package com.gemsdev.flinkapp2.base

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}