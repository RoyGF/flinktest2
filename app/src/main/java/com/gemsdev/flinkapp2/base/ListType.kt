package com.gemsdev.flinkapp2.base

enum class ListType {
    FAVORITES,
    POPULAR
}