package com.gemsdev.flinkapp2.base

import io.reactivex.ObservableTransformer

abstract class Transformer<T> : ObservableTransformer<T, T>